const express = require('express')
const mongoose = require('mongoose')
// Allows our backend application to be available to our frontend application
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoutes')

const port = 4000

const app = express()

// Allows all resources to access our backend application
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use("/users", userRoutes)
app.use("/courses", courseRoutes)
mongoose.connect("mongodb+srv://admin123:admin123@zuitt-bootcamp.cuxdw.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
})

let db = mongoose.connection

db.on('error', () => console.error.bind(console, 'error'))
db.once('open', () => console.log('Now connected to MongoDB Atlas'))

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${port}`)
})






