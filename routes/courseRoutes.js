const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require("../auth");


router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    courseController.addCourse(req.body, {id: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})

router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
});

router.get("/", (req, res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});

router.get("/:courseId", (req, res) => {

	console.log(req.params)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))

});

router.put("/:courseId", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))

})

router.put('/:courseId/archive', auth.verify, (req, res) => {

	const data = {
		courseId : req.params.courseId,
		payload : auth.decode(req.headers.authorization).isAdmin
	}

	courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController))
});

// router.put('/:courseId/archive', auth.verify, (req, res) => {

// 	const data = {
// 		courseId : req.params.courseId,
// 		payload : auth.decode(req.headers.authorization).isAdmin
// 	}

// 	courseController.archiveCourse(data, req.body).then(resultFromController => res.send(resultFromController))
// });




module.exports = router;