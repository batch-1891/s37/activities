const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({id : userData.id}).then(resultFromController => res.send(resultFromController));

});

router.post("/enroll", auth.verify, (req, res) => {
	
	let data = {
		// User ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id,
		// Course ID will be retrieved from the request body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		courseId: req.body.courseId
	}

	/*
		{
			id:
			email:
			isAdmin
		}

	*/

	userController.enroll(data).then(resultFromController => res.send(resultFromController))

});




module.exports = router;