const Course = require('../models/Course');
const User = require('../models/User');


module.exports.addCourse = (reqBody, reqId) => {

    return User.findById(reqId.id).then(result => {

        if (reqId.isAdmin == false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
        
    });    
}

module.exports.getAllCourses = async (data) => {

	if (data.isAdmin) {
		return Course.find({}).then(result => {

			return result
		})
	} else {

		return false
	}
}

module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {

		return result 
	})

}

module.exports.getCourse = (reqParams) => {

    return Course.findById(reqParams.courseId).then(result => {

        return result
    })

}

module.exports.updateCourse = (reqParams, reqBody) => {

    // Specify the fields/properties of the document to be updated
    let updatedCourse = {

        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price

    }

    // Syntax
            // findByIdAndUpdate(document ID, updatesToBeApplied)
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) =>{

        // Course not updated
        if(error) {

            return false

        // Course updated successfully
        } else {

            return true
        }
    })
}

module.exports.archiveCourse = (data) => {

    return Course.findById(data.courseId).then((result, err) => {

        if(data.payload === true) {

            result.isActive = false;

            return result.save().then((archivedCourse, err) => {

                // Course not archived
                if(err) {

                    return false;

                // Course archived successfully
                } else {


                    return true;
                }
            })

        } else {

            //If user is not Admin
            return false
        }

    })
}


// module.exports.archiveCourse = (data, reqBody) => {

//  if (data.payload === true) {

//      let updateActiveField = {
//          isActive: reqBody.isActive
//      }

//      return Course.findByIdAndUpdate(data.courseId, updateActiveField).then((course, err) => {

//              if(err) {
                
//                  return true
//              }  else {

//                  return false
//              }
//      })
         
//  } else {

//      return false
//  } 

// }


